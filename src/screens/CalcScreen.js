import React, { useState } from 'react';
import { View, Text, StyleSheet, Button, TouchableOpacity } from 'react-native';
import CalculatorButton from "../components/calculator/CalculatorButton";


const CalcScreen = () => {
    let [operationText, setOperationText] = useState('');
    const [result, setResult] = useState('0');
    const operationchars = ['%', '/', '*', '+', '-'];


    const buttonPress = (buttonValue) => {
        switch (buttonValue) {
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                setOperationText(operationText + buttonValue)
                console.log(buttonValue);
                break;
            case '+':
            case '-':
            case '*':
            case '/':
                let operationArray = operationText.split('');
                let last = operationArray.pop();
                if (operationchars.includes(last) == true) {
                    setOperationText(operationArray.join("") + buttonValue);
                } else {
                    setOperationText(operationText + buttonValue);
                }
                break;
            case '.':
                if (operationText.substr(-1) == '.') {
                    break;
                }
                setOperationText(operationText + buttonValue);
                break;
            case '%':
                setOperationText(buttonValue + operationText / 100);
                break;
            case 'AC':
                console.log('clear');
                setResult([]);
                setOperationText([]);
                break;
            case 'Del':
                operationText = operationText.split('');
                operationText.pop();
                operationText = operationText.join("");
                setOperationText(operationText);
                console.log('delate last');
                break;
            case '=':
                last = operationText.substr(-1);
                if (operationchars.includes(last) == true) {
                    setOperationText('');
                    setResult('błąd!');
                    break;
                }
                if (typeof eval(operationText) === 'undefined') {
                    break;
                }
                setOperationText(eval(operationText));
                let num = eval(operationText).toString();
                setOperationText(num);
                setResult(eval(operationText));
                console.log('result')
                break;



        }
    }

    return (

        <View style={styles.container}>
            <View style={styles.calculation}>
                <Text style={styles.result} >{result} </Text>

            </View>
            <View style={styles.results}>
                <Text style={styles.operation}>{operationText} </Text>
            </View>
            <View style={styles.buttons}>
                <View style={styles.row} >

                    <CalculatorButton styleValue={{ color: 'orange' }} styleBgc={{ backgroundColor: '#212121' }} buttonValue={'AC'} onPressButton={buttonPress} />
                    <CalculatorButton styleValue={{ color: 'orange' }} styleBgc={{ backgroundColor: '#212121' }} buttonValue={'Del'} onPressButton={buttonPress} />
                    <CalculatorButton styleValue={{ color: 'orange' }} styleBgc={{ backgroundColor: '#212121' }} buttonValue={'%'} onPressButton={buttonPress} />
                    <CalculatorButton styleValue={{ color: 'orange' }} styleBgc={{ backgroundColor: '#212121' }} buttonValue={'/'} onPressButton={buttonPress} />

                </View>
                <View style={styles.row}>
                    <CalculatorButton styleValue={{ color: 'orange' }} styleBgc={{ backgroundColor: '#404040' }} buttonValue={'7'} onPressButton={buttonPress} />
                    <CalculatorButton styleValue={{ color: 'orange' }} styleBgc={{ backgroundColor: '#404040' }} buttonValue={'8'} onPressButton={buttonPress} />
                    <CalculatorButton styleValue={{ color: 'orange' }} styleBgc={{ backgroundColor: '#404040' }} buttonValue={'9'} onPressButton={buttonPress} />
                    <CalculatorButton styleValue={{ color: 'orange' }} styleBgc={{ backgroundColor: '#212121' }} buttonValue={'*'} onPressButton={buttonPress} />


                </View>
                <View style={styles.row}>
                    <CalculatorButton styleValue={{ color: 'orange' }} styleBgc={{ backgroundColor: '#404040' }} buttonValue={'4'} onPressButton={buttonPress} />
                    <CalculatorButton styleValue={{ color: 'orange' }} styleBgc={{ backgroundColor: '#404040' }} buttonValue={'5'} onPressButton={buttonPress} />
                    <CalculatorButton styleValue={{ color: 'orange' }} styleBgc={{ backgroundColor: '#404040' }} buttonValue={'6'} onPressButton={buttonPress} />
                    <CalculatorButton styleValue={{ color: 'orange' }} styleBgc={{ backgroundColor: '#212121' }} buttonValue={'-'} onPressButton={buttonPress} />


                </View>
                <View style={styles.row}>
                    <CalculatorButton styleValue={{ color: 'orange' }} styleBgc={{ backgroundColor: '#404040' }} buttonValue={'1'} onPressButton={buttonPress} />
                    <CalculatorButton styleValue={{ color: 'orange' }} styleBgc={{ backgroundColor: '#404040' }} buttonValue={'2'} onPressButton={buttonPress} />
                    <CalculatorButton styleValue={{ color: 'orange' }} styleBgc={{ backgroundColor: '#404040' }} buttonValue={'3'} onPressButton={buttonPress} />
                    <CalculatorButton styleValue={{ color: 'orange' }} styleBgc={{ backgroundColor: '#212121' }} buttonValue={'+'} onPressButton={buttonPress} />


                </View>
                <View style={styles.row}>
                    <CalculatorButton styleValue={{ color: 'orange' }} styleBgc={{ backgroundColor: '#212121' }} buttonValue={''} onPressButton={buttonPress} />
                    <CalculatorButton styleValue={{ color: 'orange' }} styleBgc={{ backgroundColor: '#404040' }} buttonValue={'0'} onPressButton={buttonPress} />
                    <CalculatorButton styleValue={{ color: 'orange' }} styleBgc={{ backgroundColor: '#212121' }} buttonValue={'.'} onPressButton={buttonPress} />
                    <CalculatorButton styleValue={{ color: 'orange' }} styleBgc={{ backgroundColor: '#212121' }} buttonValue={'='} onPressButton={buttonPress} />


                </View>
            </View>
        </View>



    )

};
const styles = StyleSheet.create({
    container: {
        flex: 3,

    },
    results: {
        flex: 1,
        backgroundColor: 'grey',
        alignItems: "flex-end",
        justifyContent: "flex-end",
    },
    calculation: {
        flex: 2,
        backgroundColor: 'darkgrey',
        alignItems: "flex-end",
    },
    buttons: {
        flex: 5,


    },
    row: {
        flex: 1,
        flexDirection: 'row',

    },
    button: {
        flexBasis: "25%",
        justifyContent: "center",
        alignItems: "center",
    },
    bgc: {
        backgroundColor: "orange",
    },
    operation: {
        fontSize: 20,
        fontWeight: "bold",

    },
    result: {
        fontSize: 24,
        fontWeight: "bold",
    }

})

export default CalcScreen;