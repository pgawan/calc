import React from 'react';
import { Text, StyleSheet, TouchableOpacity } from 'react-native';

const CalculatorButton = (props) => {
    // console.log(props);
    return (
        <TouchableOpacity

            // style={[styles.button, props.styleValue, props.styleBgc]}
            style={[styles.button, props.styleBgc]}
            onPress={() => props.onPressButton(props.buttonValue)}
            activeOpacity={1}
        >
            <Text style={[props.styleValue]}> {props.buttonValue} </Text>
        </TouchableOpacity>
    )
}



const styles = StyleSheet.create({

    button: {
        flexBasis: "25%",
        justifyContent: "center",
        alignItems: "center",
    },

})



export default CalculatorButton;