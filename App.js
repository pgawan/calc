import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import CalcScreen from './src/screens/CalcScreen';

const MainNavigator = createStackNavigator({
  Calc: { screen: CalcScreen },

},
  {
    initialRouteName: 'Calc',
    defaultNavigationOptions: {
      title: 'Kalkulator'
    }
  }
)




export default createAppContainer(MainNavigator);


